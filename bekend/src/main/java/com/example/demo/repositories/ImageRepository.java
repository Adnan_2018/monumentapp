package com.example.demo.repositories;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.models.Image;

@Transactional
public interface ImageRepository extends JpaRepository<Image,Long> {
	
}
