package com.example.demo.repositories;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.example.demo.models.Monument;
@Transactional
public interface MonumentRepository extends JpaRepository<Monument,Long> {
	
	@Modifying
	@Query("update Monument u set u.isDeleted=true where u.id = ?1")
	void softDelete(Long id);
	
	@Query
	public List<Monument> findByIsDeletedFalse();
}

