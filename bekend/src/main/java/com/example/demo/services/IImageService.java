package com.example.demo.services;
import java.util.List;
import com.example.demo.models.Image;

public interface IImageService {
	public Image addImage(Image image);
	public Image getImageById(long id);
	public Image getLastImage();
	public Image update(Long id);
	public List<Image>getImagesByMonument(Long id);
}
