package com.example.demo.services;
import java.util.List;
import com.example.demo.models.Monument;

public interface IMonumentService {
	
	public Monument addMonument(Monument monument);
	public List<Monument> getAllMonuments();
	public Monument getMonumentyById(long id);
	public boolean deleteMonumentById(long id);
	public Monument updateMonument(Long id,Monument monument);
	public Monument getMonumentByName(String name);
	public List<Monument> getMonumentByPriority(String priority);
	public Monument setRate(Long id, int rate);
}
