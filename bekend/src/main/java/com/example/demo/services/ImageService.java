package com.example.demo.services;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.models.Image;
import com.example.demo.models.Monument;
import com.example.demo.repositories.ImageRepository;
import com.example.demo.repositories.MonumentRepository;
@Service
public class ImageService implements IImageService {
	
	@Autowired
	private ImageRepository imageRepository;
	private MonumentRepository monumentRepository;
	
	public ImageService(ImageRepository imageRepository, MonumentRepository monumentRepository) {
		this.imageRepository = imageRepository;
		this.monumentRepository = monumentRepository;
	}
	
	public Image addImage(Image image) {
		return imageRepository.save(image);
	}
	
	public Image getImageById(long id) {
		Image targetImage = imageRepository.findById(id).orElse(null);
		if(targetImage==null){
			return null;
		}
		return targetImage;
	}
	
	public Image getLastImage() {
		List<Image> listOfImages = imageRepository.findAll();
		return listOfImages.get(listOfImages.size()-1);
	}
	
	public Image update(Long id) {
		List<Image> listOfImages = imageRepository.findAll();
		Monument monument = monumentRepository.findById(id).orElse(null);
		if(listOfImages.size()==0) {
			return null;
		}
		Image image = listOfImages.get(listOfImages.size()-1);
		image.setMonument(monument);
		return imageRepository.save(image);
	}
	
	
	public List<Image>getImagesByMonument(Long id) {
		Monument monument = monumentRepository.findById(id).orElse(null);
		List<Image> listOfImages = imageRepository.findAll();
		List<Image> toReturnArray = new ArrayList<>();
		for(int i=0;i<listOfImages.size();i++) {
			if(listOfImages.get(i).getMonument()!=null) {
				if(listOfImages.get(i).getMonument().equals(monument)) {
					toReturnArray.add(listOfImages.get(i));
				}
			}
		}
		return toReturnArray;
	}
}
