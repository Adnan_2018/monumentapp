package com.example.demo.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.models.Monument;
import com.example.demo.repositories.MonumentRepository;
import java.util.ArrayList;
import java.util.List;

@Service
public class MonumentService implements IMonumentService{
	
	@Autowired
	private MonumentRepository monumentRepository;
	
	public MonumentService(MonumentRepository monumentRepository) {
		this.monumentRepository = monumentRepository;
	}
	
	public Monument addMonument(Monument monument) {
		return monumentRepository.save(monument);
	}
	
	public List<Monument> getAllMonuments(){
		return monumentRepository.findByIsDeletedFalse();
	}
	
	public Monument getMonumentyById(long id) {
		Monument targetMonument = monumentRepository.findById(id).orElse(null);
		if(targetMonument==null || targetMonument.isDeleted()){
			return null;
		}
		return targetMonument;
	}
	
	public boolean deleteMonumentById(long id) {
		Monument monument = monumentRepository.findById(id).orElse(null);
		if(monument == null || monument.isDeleted()==true) {
			return false;
		}
		monumentRepository.softDelete(id);
		return true;
	}
	
	public Monument updateMonument(Long id,Monument monument) {
		Monument oldMonument = monumentRepository.findById(id).orElse(null);
		if(oldMonument == null || oldMonument.isDeleted()==true){
            return null;
        }
		oldMonument.setName(monument.getName());
		oldMonument.setDescription(monument.getDescription());
		oldMonument.setPriority(monument.getPriority());
		oldMonument.setDeleted(monument.isDeleted());
		oldMonument.setLon(monument.getLon());
		oldMonument.setLan(monument.getLan());
		return monumentRepository.save(oldMonument);
	}
	
	public Monument getMonumentByName(String name) {
		List<Monument> list = getAllMonuments();
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getName().equals(name)) {
				return list.get(i);
			}
		}
		return null;
	}
	
	public List<Monument> getMonumentByPriority(String priority){
		List<Monument> list = getAllMonuments();
		List<Monument> listByPriority = new ArrayList<>();
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getPriority().equals(priority)) {
				listByPriority.add(list.get(i));
			}
		}
		return listByPriority;
	}
	
	public Monument setRate(Long id, int rate) {
		Monument monument = monumentRepository.findById(id).orElse(null);
		
		if(monument == null || monument.isDeleted() == true) {
			return null;
		}
		if(monument.getNumberOfRates() == 0) {
			monument.setAverageRate(rate);
			monument.setNumberOfRates(monument.getNumberOfRates()+1);
		}else if(monument.getNumberOfRates() == 1) {
			monument.setAverageRate((monument.getAverageRate()+rate)/2);
			monument.setNumberOfRates(monument.getNumberOfRates()+1);
		}else {
			monument.setAverageRate((monument.getNumberOfRates()*monument.getAverageRate()+rate)/(monument.getNumberOfRates()+1));
			monument.setNumberOfRates(monument.getNumberOfRates()+1);
		}
		return monumentRepository.save(monument);
	}
}
