package com.example.demo.models;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "monument")
public class Monument implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false, updatable=false)
	private Long id;
	private String name;
	@Column(length = 4000)
	private String description;
	private String priority;
	private float averageRate;
	private double lan;
	private double lon;
	private boolean isDeleted = false;
	private int numberOfRates = 0;

	@OneToMany(mappedBy = "monument")
    private Set<Image> images;

	public Monument(String name, String description, float averageRate, double lan, double lon,
			boolean isDeleted, String priority, int numberOfRates) {
		super();
		this.name = name;
		this.description = description;
		this.averageRate = averageRate;
		this.lan = lan;
		this.lon = lon;
		this.isDeleted = isDeleted;
		this.priority = priority;
		this.numberOfRates = numberOfRates;
	}
	
	public Monument() {
		super();
	}
	public Monument(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getAverageRate() {
		return averageRate;
	}
	public void setAverageRate(float averageRate) {
		this.averageRate = averageRate;
	}
	public double getLan() {
		return lan;
	}
	public void setLan(double lan) {
		this.lan = lan;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

	public int getNumberOfRates() {
		return numberOfRates;
	}

	public void setNumberOfRates(int numberOfRates) {
		this.numberOfRates = numberOfRates;
	}
}
