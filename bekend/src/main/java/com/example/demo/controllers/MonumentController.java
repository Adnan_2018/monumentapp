package com.example.demo.controllers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.services.IMonumentService;
import com.example.demo.models.Monument;

@RestController
@RequestMapping("/monuments")
public class MonumentController {
	
	@Autowired
	private IMonumentService monumentService;
	
	public MonumentController(IMonumentService monumentService) {
		this.monumentService = monumentService;
	}
	
	@PostMapping("/addMonument")
	public ResponseEntity<Void> addMonument(@RequestBody Monument monument){
		monumentService.addMonument(monument);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Monument>> getAllMonuments(){
		return new ResponseEntity<>(monumentService.getAllMonuments(),HttpStatus.OK);
	}
	
	@GetMapping("/monument/{id}")
	public ResponseEntity<Monument> getMonumentById(@PathVariable("id") Long id) {
		if(monumentService.getMonumentyById(id) == null) {
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(monumentService.getMonumentyById(id),HttpStatus.OK);
	}
	
	@PutMapping("/updateMonument/{id}")
	public ResponseEntity<Void> updateMonument(@PathVariable("id") Long id,@RequestBody Monument monument) throws Exception{
		if(monumentService.updateMonument(id,monument) != null) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/deleteMonument/{id}")
	public ResponseEntity<Void> deleteMonumentById(@PathVariable("id") Long id) throws Exception{
		if(monumentService.deleteMonumentById(id)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/getByName/{name}")
	public ResponseEntity<Monument> getMonumentByName(@PathVariable("name")String name){
		Monument monument = monumentService.getMonumentByName(name);
		if(monument == null) {
			Monument m = new Monument("Not found");
			return new ResponseEntity<>(m,HttpStatus.OK);
		}
		return new ResponseEntity<>(monument,HttpStatus.OK);
	}
	
	@GetMapping("/getByPriority/{priority}")
	public ResponseEntity<List<Monument>> getMonumentsByPriority(@PathVariable("priority") String priority){
		
		return new ResponseEntity<>(monumentService.getMonumentByPriority(priority), HttpStatus.OK);
	}
	
	@PutMapping("/rateMonument/{id}")
	public ResponseEntity<Void> rateMonument(@PathVariable("id") Long id, @RequestBody int rate){
		if(monumentService.setRate(id,rate)!=null) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
