package com.example.demo.controllers;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.example.demo.models.Image;
import com.example.demo.services.IImageService;

@RestController
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	private IImageService imageService;
	
	public ImageController(IImageService imageService) {
		this.imageService= imageService;
	}
	
	@PostMapping("/uploadImage")
	public ResponseEntity<Image> addImage(@RequestParam("imageFile") MultipartFile file) throws IOException{
		Image image = new Image(file.getOriginalFilename(), file.getContentType(),file.getBytes());
		imageService.addImage(image);
		return new ResponseEntity<>(image,HttpStatus.OK);
	}
	
	@PutMapping("/update")
	public ResponseEntity<Void> update(@RequestBody Long id){
		imageService.update(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/getImages/{id}")
	public ResponseEntity<List<Image>> getImagesByMonument(@PathVariable("id") Long id){
		return new ResponseEntity<>(this.imageService.getImagesByMonument(id),HttpStatus.OK);
	}
}
