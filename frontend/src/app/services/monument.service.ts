import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonumentService {

  //First example of possible ways to define path
  baseUrl='http://localhost:8080/monuments';
  deleteMonumentUrl: string = this.baseUrl + '/deleteMonument';

  constructor(private http: HttpClient){}

  //Function that calls api for adding new monuments, with defining path
  create(data:any): Observable<any> {
    const baseUrl = 'http://localhost:8080/monuments/addMonument';
    return this.http.post(baseUrl, data);
  }

  //Function that calls api to get all active monuments, with defining path
  getAll(){
    const baseUrl = 'http://localhost:8080/monuments';
    return this.http.get(baseUrl);
  }

  //Function that calls api to delete monument, with already defined path
  deleteMonument(id:any){
    const basrUrl=`${this.deleteMonumentUrl}/${id}`;
    return this.http.delete<void>(basrUrl,id);
  }

  //Function that calls api to update monument
  updateMonument(id:any,data:any):Observable<any>{
    const baseUrl='http://localhost:8080/monuments/updateMonument/'+id;
    return this.http.put(baseUrl,data);
  }

  //Function that calls api to get monument by name
  getMonumentByName(name:any){
    const baseUrl = 'http://localhost:8080/monuments/getByName/'+name;
    return this.http.get(baseUrl);
  }

  //Function that calls api to get monuments by priority
  getMonumentsByPriority(priority:any){
    const baseUrl='http://localhost:8080/monuments/getByPriority/'+priority;
    return this.http.get(baseUrl);
  }

  //Function that calls api to rate monument
  rateMonument(id:any,rate:any): Observable<any>{
    const baseUrl='http://localhost:8080/monuments/rateMonument/'+id;
    return this.http.put(baseUrl,rate);
  }
}
