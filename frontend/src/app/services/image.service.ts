import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient) { }

    //Function that calls api to upload image
    uploadImage(data:any): Observable<any>{
      const baseUrl = 'http://localhost:8080/image/uploadImage';
      return this.http.post(baseUrl, data);
    }
  
    //Function that calls api to update foreign key of last image, they are null after iamge being uploaded
    updateImage(id:any){
      const baseUrl= 'http://localhost:8080/image/update';
      return this.http.put(baseUrl,id);
    }

    //Function that calls api to get images, parameter is monument id
    getImages(id:any){
      const baseUrl='http://localhost:8080/image/getImages/'+id;
      return this.http.get(baseUrl);
    }
}
