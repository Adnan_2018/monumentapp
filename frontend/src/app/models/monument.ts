export class Monument{
    id: number;
    name: string;
    description: string;
    priority: string;
    averageRate: number;
    isDeleted: boolean;
    lan: number;
    lon: number;
   constructor(response?:any){
       this.id=response.id;
       this.name=response.name;
       this.description=response.description;
       this.priority=response.priority;
       this.averageRate=response.averageRate;
       this.isDeleted=response.isDeleted;
       this.lan=response.lan;
       this.lon=response.lon;
   }
}