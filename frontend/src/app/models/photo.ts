import { Monument } from '../models/monument';
export class Photo{
    id: number;
    name: string;  
    type: string;
    picByte: [];
    monument: Monument;
    constructor(response:any){
        this.id=response.id;
        this.name=response.name;
        this.type=response.type;
        this.picByte=response.picByte;
        this.monument=response.monument;
    }
}