import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core';
import {ItemListComponent} from './components/item-list/item-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ItemListComponent
  ],
  entryComponents:[],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    NgbModule,
    NgbModalModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut:4000,
      progressBar:true,
      progressAnimation:'increasing',
      preventDuplicates:true
    }),
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyArEQY0zzGa3cl2mWGq-cgzpOOsY2YR9fE'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
