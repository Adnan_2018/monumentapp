import { Component, NgZone, OnInit} from '@angular/core';
import { MonumentService } from 'src/app/services/monument.service';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  //Array of possible priorities
  Priorities: any = ['You have to see','If you have time','Not so important'];

  //Variables used for Google maps implementation
  map: any;
  mapClickListener: any;
  
  //Variable used for rating
  rating = 0;

  //Default value for search criteria label
  criteria='Choose criteria';

  //Text to search if criteria equal to 'By name'
  searchText:any;

  //Hold value of currently selected monument
  currentMonument:any;

  //Defult monument object used for adding new monument form (binding)
  monument  = {
    name:'',
    description:'',
    lan:43.8563,
    lon:18.4131,
    priority:''
  };

  //Arrays for monuments and images
  arrayOfMonuments:any=[];
  arrayOfImages:any=[];

 //Array of possible seacrh criteries
  Criteries: any = ['By name','By priority','Show all'];

  //Default value for search priority label
  priorityToSearch='Choose priority to search';

  //Variables used for search feature
  searchedMonument:any;
  notFoundBoolean:boolean = false;

  //Variable that holds selected file
  selectedFile: any;

  //Response message depending on success or fail upload
  message='';

  constructor(private monumentService:MonumentService, 
             private imageService:ImageService,
             private modalService: NgbModal,
             private toastr:ToastrService,
             private zone: NgZone) {}

  ngOnInit() {
    this.getAll();
  }

  //Function that fetch all active monuments using service method getAll(), and store in array
  getAll(){
    this.monumentService.getAll().subscribe((response)=>{
      this.arrayOfMonuments = response;
    });
  }

  //Function that opens modal, and set selected monument to current monument 
  triggerModal(content:any, monument:any) {
    this.modalService.open(content);
    this.currentMonument=monument;
  }

  //Function that reset value of input fields
  clearForm(){
    this.monument.name='';
    this.monument.description='';
    this.monument.lan=43.8563;
    this.monument.lon=18.4131;
    this.monument.priority='';
  }

  //Function that close modal and call function clearForm()
  closeModal(){
    this.modalService.dismissAll();
    this.clearForm();
  }

  //Function used to open modal for rating monuments
  triggerModalAlone(content:any) {
    this.modalService.open(content);
  }

  //Function used to open modal and display images
  triggerModalForImages(content:any,monument:any) {
    this.modalService.open(content);
    this.currentMonument = monument;
    this.imageService.getImages(this.currentMonument.id).subscribe((response)=>{
      this.arrayOfImages = response;
    })
  }

  //Function that save new monument
  saveMonument(){
    const data = {
      name: this.monument.name,
      description: this.monument.description,
      lan:this.monument.lan,
      lon:this.monument.lon,
      priority:this.monument.priority
    };
    //Call function create() from service 
    this.monumentService.create(data)
      .subscribe(
        response => {
          this.getAll();
          console.log(response);
        },
        error => {
          console.log(error);
        });
    this.clearForm();
    this.modalService.dismissAll(); // Close modal
    this.showToastrForAdding();
  }

  //Function used to display pop up message after adding new monument
  showToastrForAdding(){
    this.toastr.success('New monument has been successfully added','Monument added');
  }

  //Function that update monument
  updateMonument(){
    const data = {
      name: this.currentMonument.name,
      description: this.currentMonument.description,
      priority:this.currentMonument.priority,
      lan:this.currentMonument.lan,
      lon:this.currentMonument.lon
    };
    //Call function updateMonument() from service 
    this.monumentService.updateMonument(this.currentMonument.id,data)
      .subscribe(
        response => {
          console.log(response);
          this.getAll();
        },
        error => {
          console.log(error);
        });
    this.modalService.dismissAll(); // Close modal
    this.showToastrForUpdate();
    this.updateId();
    this.message='';
  }

  //Function used to display pop up message after update
  showToastrForUpdate(){
    this.toastr.success('Update has been done','Update');
  }

  //Function used to delete currently selected monument
  deleteMonument(){
    this.monumentService.deleteMonument(this.currentMonument.id).subscribe(response =>{
        this.getAll();
    });
    this.closeModal();
  }

  //Function used to display Google Maps location in Review modal
  location(map: google.maps.Map){
    this.map = map;
    this.mapClickListener = this.map.addListener('click', (e: google.maps.MouseEvent) => {
      this.zone.run(() => {
        this.currentMonument.lon=e.latLng.lng();
        this.currentMonument.lan=e.latLng.lat();
      });
    });
  }

  //Function used to select location from Google Maps
  locationForAdding(map: google.maps.Map){
    this.map = map;
    this.mapClickListener = this.map.addListener('click', (e: google.maps.MouseEvent) => {
      this.zone.run(() => {
        this.monument.lon=e.latLng.lng();
        this.monument.lan=e.latLng.lat();
      });
    });
  }

  //Function used to searh monument depending on criteria
  searchMonument(){
    if(this.criteria === this.Criteries[0]){
      this.monumentService.getMonumentByName(this.searchText).subscribe((response)=>{
        this.searchedMonument = response;
        if(this.searchedMonument.name === 'Not found'){
          this.arrayOfMonuments=[];
          this.notFoundBoolean = true;
        }else{
          this.arrayOfMonuments = [];
          this.arrayOfMonuments[0] = this.searchedMonument;
          this.notFoundBoolean = false;
        }
      })
    }else if(this.criteria===this.Criteries[1]){
      this.monumentService.getMonumentsByPriority(this.priorityToSearch).subscribe((response)=>{
      this.arrayOfMonuments = [];
      this.arrayOfMonuments = response; 
      if(this.arrayOfMonuments.length === 0){
        this.notFoundBoolean = true;
      }else{
        this.notFoundBoolean = false;
      }
      })
    }else if(this.criteria===this.Criteries[2]){
      this.notFoundBoolean =false;
      this.getAll();
    }
  }

  //Function that save currenlty selected file
  fileChange(event:any){
      this.selectedFile = event.target.files[0];
  }

  //Function used to upload image 
  onUpload(){
    if(this.selectedFile==null){
      this.message='Please choose an image';
    }
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
    this.imageService.uploadImage(uploadImageData).subscribe((response)=>{
      if(response!== null){
        this.message='Image uploaded';
      }else{
        this.message='Something wrong';
      }
    })  
  }

  //Function used to update foreign key of last image, since they are null in beggining
  updateId(){
    this.imageService.updateImage(this.currentMonument.id).subscribe((response)=>{
    })
  }

  //Function used to rate monument, parameters are id of monument and rate value
  rateMonument(id:any,rate:any){
    this.monumentService.rateMonument(id,rate).subscribe((response)=>{
      this.modalService.dismissAll();
      this.getAll();
    })
  }
}
